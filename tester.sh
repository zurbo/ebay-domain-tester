#! /bin/bash

DIR=$(dirname $0)
PREFIXES=$(cat $DIR/prefixes)
LANGUAGES=$(cat $DIR/languages)
DOMAINS=$(cat $DIR/domains)
SUFFIXES=$(cat $DIR/suffixes)
RES="x"
STRT=""
DONE=""
if [ -f $DIR/done ]
then
	DONE=$(cat $DIR/done)
fi
for DOMAIN in $DOMAINS
do
	if ! grep "\." <<< $DOMAIN &> /dev/null
	then
		for SUFFIX in $SUFFIXES
		do
			if ! grep $DOMAIN.$SUFFIX <<< $DONE &> /dev/null
			then
				ping -c 1 -s 0 -W 1 $DOMAIN.$SUFFIX &> /dev/null
				if [[ $? -ne 2 ]]
				then
					echo -ne $STRT "\"$DOMAIN.$SUFFIX\"" >> $DIR/list
					STRT=", "
					RES="✔"
				fi
				echo -e "$DOMAIN.$SUFFIX $RES" >> $DIR/done
				RES="x"
			fi
			for PREFIX in $PREFIXES
			do
				if ! grep $PREFIX.$DOMAIN.$SUFFIX <<< $DONE &> /dev/null
				then
					ping -c 1 -s 0 -W 1 $PREFIX.$DOMAIN.$SUFFIX &> /dev/null
					if [[ $? -ne 2 ]]
		 			then
						echo -ne $STRT "\"$PREFIX.$DOMAIN.$SUFFIX\"" >> $DIR/list
						STRT=", "
						RES="✔"
					fi
					echo -e "\t$PREFIX.$DOMAIN.$SUFFIX $RES" >> $DIR/done
					RES="x"
				fi
			done
			for LANGUAGE in $LANGUAGES
			do
				if grep "^$SUFFIX" <<< $LANGUAGE &> /dev/null
				then
					if ! grep $LANGUAGE.$DOMAIN.$SUFFIX <<< $DONE &> /dev/null
					then
						ping -c 1 -s 0 -W 1 $LANGUAGE.$DOMAIN.$SUFFIX &> /dev/null
						if [[ $? -ne 2 ]]
			 			then
							echo -ne $STRT "\"$LANGUAGE.$DOMAIN.$SUFFIX\"" >> $DIR/list
							STRT=", "
							RES="✔"
						fi
						echo -e "\t$LANGUAGE.$DOMAIN.$SUFFIX $RES" >> $DIR/done
						RES="x"
					fi
					for PREFIX in $PREFIXES
					do
						if ! grep $PREFIX.$LANGUAGE.$DOMAIN.$SUFFIX <<< $DONE &> /dev/null
						then
							ping -c 1 -s 0 -W 1 $PREFIX.$LANGUAGE.$DOMAIN.$SUFFIX &> /dev/null
							if [[ $? -ne 2 ]]
				 			then
								echo -ne $STRT "\"$PREFIX.$LANGUAGE.$DOMAIN.$SUFFIX\"" >> $DIR/list
								STRT=", "
								RES="✔"
							fi
							echo -e "\t\t$PREFIX.$LANGUAGE.$DOMAIN.$SUFFIX $RES" >> $DIR/done
							RES="x"
						fi
					done
				fi
			done
		done
	else
		if ! grep $DOMAIN <<< $DONE &> /dev/null
		then
			ping -c 1 -s 0 -W 1 $DOMAIN &> /dev/null
			if [[ $? -ne 2 ]]
			then
				echo -ne $STRT "\"$DOMAIN\"" >> $DIR/list
				STRT=", "
				RES="✔"
			fi
			echo -e "$DOMAIN $RES" >> $DIR/done
			RES="x"
		fi
		for PREFIX in $PREFIXES
		do
			if ! grep $PREFIX.$DOMAIN <<< $DONE &> /dev/null
			then
				ping -c 1 -s 0 -W 1 $PREFIX.$DOMAIN &> /dev/null
				if [[ $? -ne 2 ]]
				then
					echo -ne $STRT "\"$PREFIX.$DOMAIN\"" >> $DIR/list
					STRT=", "
					RES="✔"
				fi
				echo -e "\t$PREFIX.$DOMAIN $RES" >> $DIR/done
				RES="x"
			fi
		done
	fi
done
